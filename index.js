// require the dependencies
const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator");

// set the port for the app
const port = process.env.PORT || parseInt(process.argv.pop()) || 3000;

// make an array of 3 plant products
const products = [
  {
    id: "noFiddle",
    image: "fiddle",
    name: "Fiddle Leaf Fig",
    info: "Giant green leaves with lots of cells.",
    cost: 2.04,
  },
  {
    id: "noFicus",
    image: "ficus",
    name: "Ficus Audrey",
    info: "Lush leaves with white pinstripes.",
    cost: 1.41,
  },
  {
    id: "noMonstera",
    image: "monstera",
    name: "Monstera Deliciosa",
    info: "Species of evergreen tropical vines.",
    cost: 3.24,
  },
];

// make an array of the provinces in Canada
const provinces = [
  {
    name: "Alberta",
    tax: 5,
  },
  {
    name: "British Columbia",
    tax: 12,
  },
  {
    name: "Manitoba",
    tax: 12,
  },
  {
    name: "New Brunswick",
    tax: 15,
  },
  {
    name: "Newfoundland and Labrador",
    tax: 15,
  },
  {
    name: "Northwest Territories",
    tax: 5,
  },
  {
    name: "Nova Scotia",
    tax: 15,
  },
  {
    name: "Nunavut",
    tax: 5,
  },
  {
    name: "Ontario",
    tax: 13,
  },
  {
    name: "Prince Edward Island",
    tax: 15,
  },
  {
    name: "Quebec",
    tax: 14.975,
  },
  {
    name: "Saskatchewan",
    tax: 11,
  },
  {
    name: "Yukon",
    tax: 5,
  },
];

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// set path to public folders and view folders
app.set("views", path.join(__dirname, "views"));

// set the template engine
app.set("view engine", "ejs");

//use public folder for CSS, Images etc.
app.use(express.static(__dirname + "/public"));

// home route showing the products for purchase
app.get("/", (req, res) => {
  res.render("product", {
    errors: null,
    receipt: null,
    provinces,
    products,
    form: {
      noFiddle: "0",
      noFicus: "0",
      noMonstera: "0",
      custName: "",
      custPhone: "",
      custEmail: "",
      custAddress: "",
      custCity: "",
      custProvince: "",
    },
  });
});

// route for processing and validating the product purchase request
app.post(
  "/process",
  [
    check(
      "noFiddle",
      "Fiddle plant quantity must be a number (0 and above)"
    ).isNumeric({ no_symbols: true }),
    check(
      "noFicus",
      "Ficus plant quantity must be a number (0 and above)"
    ).isNumeric({ no_symbols: true }),
    check(
      "noMonstera",
      "Monstera plant quantity must be a number (0 and above)"
    ).isNumeric({ no_symbols: true }),
    check("custName", "Customer name is required").notEmpty(),
    check(
      "custPhone",
      "Customer phone number must be a valid phone number"
    ).isMobilePhone(),
    check("custEmail", "Customer email must be a valid email").isEmail(),
    check("custAddress", "Customer address is required").notEmpty(),
    check("custCity", "Customer city is required").notEmpty(),
    check("custProvince", "Customer province is required").notEmpty(),
  ],
  (req, res) => {
    const { errors } = validationResult(req);

    // check if any error was found
    const errorInfo = {};

    if (errors.length) {
      // convert the errors array to key-value pair for proper display
      errors.forEach((err) => {
        errorInfo[err.param] = err.msg;
      });

      // return to the product page with the errors object
      return res.render("product", {
        errors: errorInfo,
        provinces,
        products,
        form: req.body,
        receipt: null,
      });
    }

    /* CALCULATE THE PURCHASE DETAILS */
    // get the request data
    const {
      noFiddle,
      noFicus,
      noMonstera,
      custName,
      custPhone,
      custEmail,
      custAddress,
      custCity,
      custProvince,
    } = req.body;

    const items = { noFiddle, noFicus, noMonstera };
    const receiptItems = [];
    const receiptTotals = [];
    const leastTotalAmount = 10;
    let totalNet = 0;

    // calclate the indivdual item total cost
    for (const item in items) {
      if (Object.hasOwnProperty.call(items, item)) {
        const qty = Number(items[item]);

        // the product details from the products array above
        const product = products.find((p) => p.id == item);

        if (qty > 0) {
          const plantTotalCost = Number((qty * product.cost).toFixed(2));
          totalNet += plantTotalCost;

          // add the plant to the receipt
          receiptItems.push({
            totalCost: plantTotalCost,
            qty: qty,
            name: product.name,
            unitPrice: product.cost,
          });
        }
      }
    }

    // if the total products bought is less that the leastTotalAmount ($10), return error
    if (totalNet < leastTotalAmount) {
      errorInfo["prodAmount"] = "The minimum purchase should be $10.";

      // return to the product page with the errors object
      return res.render("product", {
        errors: errorInfo,
        provinces,
        products,
        form: req.body,
        receipt: null,
      });
    }

    // add the sub-total deatils to the receipt totals array
    receiptTotals.push({
      desc: "Sub total before tax",
      amount: Number(totalNet.toFixed(2)),
    });

    // calculate the tax based on the selected province
    const province = provinces[custProvince];
    const tax = Number(((province.tax * totalNet) / 100).toFixed(2));
    totalNet += tax;

    // add the tax details to the receipt totals array
    receiptTotals.push({
      desc: `Total tax (${province.tax}%)`,
      amount: tax,
    });

    // add the total sales details to the receipt totals array
    receiptTotals.push({
      desc: "Total Sales",
      amount: Number(totalNet.toFixed(2)),
    });

    // build the receipt data
    const receipt = {
      custName,
      custPhone,
      custEmail,
      custAddress,
      custCity,
      custProvince: `${province.name} (${province.tax}%  Tax)`,
      items: receiptItems,
      totals: receiptTotals,
    };

    // return to the product page with the receipt object
    return res.render("product", {
      errors: null,
      provinces,
      products,
      form: req.body,
      receipt,
    });
  }
);

// start the app at the specified port
app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
