Url: https://emmyplantstore.herokuapp.com
My name is Emmanuel Ajiboye
This is  a web application for a self-order kiosk called JJ's Plant Shop. The type of store is designed to display plant products.
•	The web application displays the products available to purchase along with a button each to add the product to the order.
•	5 products is available for purchase. 
•	When the user clicks on the button to add a product to the order,it ask the user for the product quantity in a pop up.
•	When the user clicks on the checkout button,it ask the customer for their name in a pop up to place the order. 
•	The following user input must is validated:
•	Product quantity which should be numeric only. 
•	The user must enter their name to checkout. 
•	On successful checkout,a receipt will be generated for the customer, including all the products bought, quantity, cost, taxes, total cost and customer name. The receipt should only show the products the user bought. No empty or 0 (zero) values to be included. 
•	It Calculate and include GST which is (13%). Customer can print the receipt.